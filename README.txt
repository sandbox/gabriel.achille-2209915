CONTENT OF THIS FILE
--------------------

* About this module
* Configuration
* Usage

ABOUT THIS MODULE
-----------------
This module fixes some accessibility errors in form rendered by Drupal API and
Webform module. It includes:
* Drupal core Form API:
    * fieldset: set the label to invisible instead of none when "Hide label" is ticked.
    * Replace the * by (required) for required field
    * add a field formatter for image that remove the alt text (necessary for certain
decorative use of image)
    * file : Add the accessible attribute aria-live to the dynamically updated region
where error message get displayed. (in case of wrong file type)
* Drupal core Poll module:
    * remove unecessary label tag wrapping the radios input.
* Webform:
    * for all fields: wrap the description (if present) in a span with id and reference
this id in the attribute aria-describedby of the input
    * fieldset:
        * set the label to invisible instead of none when "Hide label" is ticked.
        * Replace fieldset element by a heading (optional)
    * for multiple-elements (radios, checkboxes, select_or_other, managed_file,
webform_time, date) :
replace  <div><label for="blabla">MY TITLE</label>...
by <fieldset><legend><span>MY TITLE</span></legend>...
    * select_or_other:
        * attach the title to the wrapper element instead to the
radios/checkbox/list element.
        * simplify the label of "other" field by not repeating the select title in it.
        * remove the action change focus JS action when option other is selected
    * file_managed: add a title attribute to the upload input element
    * webform_time: add hidden label to hour and to minute input.
* date_views:
    * remove duplicate id and fix orphan label for date_popup exposed view filter.

CONFIGURATION
-------------
Most of the fix listed above are automatically enabled when the module is enabled.
For the other have a look on the admin page:
    admin/config/user-interface/a11y-form

USAGE
-----
Enable the module.
