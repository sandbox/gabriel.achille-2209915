<?php

/**
 * Settings form.
 */
function a11y_form_settings_form($form, &$form_state) {
  $form['webform'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webform'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['webform']['a11y_form_webform_fieldset_html_tag'] = array(
    '#type' => 'radios',
    '#title' => t('HTML tag element for fieldset'),
    '#description' => t('Form API and Webform module make intensive use of fieldset. Too much for accessibility. Here you can replace the actual fieldset element in use in component by a heading tag (from h2 to h6 depending on the depth.).'),
    '#default_value' => variable_get('a11y_form_webform_fieldset_html_tag', 'fieldset'),
    '#options' => array('fieldset' => 'Fieldset', 'heading' => 'HTML Heading'),
  );

  return system_settings_form($form);
}

