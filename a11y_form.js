/**
 * @file a11y_form.js
 */

(function ($) {

  /**
   * The Drupal behaviors for a11y_form.
   */
  Drupal.behaviors.a11y_form = {
    attach: function(context, settings) {
      // Add the accessible attribute aria-live to the dynamically updated content
      // (http://www.w3.org/TR/wai-aria-practices/#liveprops)
      $('div.form-managed-file', context).once('a11y_form_file_upload', function () {
        console.log('attaching...');
        $(this).bind('DOMSubtreeModified', function (event){
          $('.file-upload-js-error').attr('aria-live', 'polite');
        })
      });
    }
  };

})(jQuery);
